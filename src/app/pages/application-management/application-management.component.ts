import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { NewApplicationComponent } from './new-application/new-application.component';
import { ApplicationModel } from '../../shared/model/application-model'
import { ApplicationServiceService } from '../../shared/service/firebase-api/application/application-service.service'
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-application-management',
  templateUrl: './application-management.component.html',
  styleUrls: ['./application-management.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ApplicationManagementComponent implements OnInit {
  public newApplication: ApplicationModel
  public applicationList = [];
   
  constructor(
    private dialogService: NbDialogService,
    public ApplicationService : ApplicationServiceService,
    public af:AngularFirestore,
  ) {
   }  
  ngOnInit() {
    let appList = this.af.collection(this.ApplicationService.path).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as any;
        const docId = a.payload.doc.id;
        data.selected=false
        return { docId, ...data };
      }))
    );
    appList.subscribe(r => {
      this.applicationList = r;
    }) 
  }
  Add() {
    this.newApplication = new ApplicationModel();
    this.dialogService.open(NewApplicationComponent, { context: 'Add' })
  }

  Update(){
    this.newApplication['type'] = 'update'
    this.dialogService.open(NewApplicationComponent,{context: {
      InfoData:this.newApplication
    }})
  }
  checked(checkmark){
     for (let index = 0; index < this.applicationList.length; index++) {
      const element: any = this.applicationList[index];
      element.selected = false
    }
    checkmark.selected = !checkmark.selected;
    this.newApplication = checkmark;

  }
  deleteConfirm(){
    if(confirm("Are you sure to delete applicatoin")) {      
      console.log(this.newApplication.docId)
      this.ApplicationService.delete(this.newApplication.docId).then(() => {
        this.newApplication = new ApplicationModel();
          console.log('data deleted successfully')
        }).catch((err)=>{
          console.log(err)
        })
    }
  } 
}
