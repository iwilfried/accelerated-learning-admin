import { Component, OnInit, ViewEncapsulation ,Input, ViewChild  } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ApplicationModel } from '../../../shared/model/application-model'
import { ApplicationServiceService } from '../../../shared/service/firebase-api/application/application-service.service'

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewApplicationComponent implements OnInit {
  @Input()  InfoData  : any;
  public newApplication: ApplicationModel;
  public appSelected:any
  constructor(
    public ref: NbDialogRef<NewApplicationComponent>,
    public ApplicationService : ApplicationServiceService
  ) {   }

  ngOnInit() {
    console.log("ngOnInit")
    if(!!this.InfoData){
      this.newApplication = this.InfoData
    }else{
      this.newApplication = new ApplicationModel
    }
  }
  close() {
    console.log("close")
    this.ref.close();
  }
  submitNewPoint(name) {
    console.log("submitNewPoint")
    if(!!this.InfoData && this.InfoData.type == 'update'){
      delete this.newApplication['type'];
      if(this.newApplication.application_name){
        this.ApplicationService.update(this.newApplication).then(() => {
          console.log('successfully updated')
        }).catch(err =>{
          console.log(err)
        })
        this.ref.close(name)
      }else{
        console.log("Please Enter Name")
      } 
    }else{
      console.log(this.newApplication)
      if(this.newApplication.application_name){
        this.ApplicationService.addModel(this.newApplication).then(() => {
          console.log('successfully updated')
        }).catch(err =>{
          console.log(err)
        })
        this.ref.close(name)
      }else{
        console.log("Please Enter Name")
      } 
    }
  }
}
