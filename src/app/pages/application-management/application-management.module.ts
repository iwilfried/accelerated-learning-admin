import { NgModule } from '@angular/core';
import {
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbInputModule,
} from '@nebular/theme';

import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ApplicationManagementComponent } from './application-management.component'
import { NewApplicationComponent } from './new-application/new-application.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NbCardModule,
        NbButtonModule,
        NbCheckboxModule,
        NbInputModule,
    ],
    declarations: [
        ApplicationManagementComponent,
        NewApplicationComponent

    ],
    entryComponents: [NewApplicationComponent,],
    providers:[]
})
export class ApplicationManagementModule { }