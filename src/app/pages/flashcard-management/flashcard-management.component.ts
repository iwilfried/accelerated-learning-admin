import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { NewFlashcardComponent } from './new-flashcard/new-flashcard.component';
import { FlashcardModel } from '../../shared/model/flashcard-model'
import { FlashcardService } from '../../shared/service/firebase-api/flashcard/flashcard-service'

import { ApplicationServiceService } from '../../shared/service/firebase-api/application/application-service.service'
import { SubjectService } from '../../shared/service/firebase-api/subject/subject-service'
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { TopicModel } from '../../shared/model/topic-model'
import { TopicService } from '../../shared/service/firebase-api/topic/topic-service'

@Component({
  selector: 'app-flashcard-management',
  templateUrl: './flashcard-management.component.html',
  styleUrls: ['./flashcard-management.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class FlashcardManagementComponent implements OnInit {

  public newFlashcard: FlashcardModel
  public TopicList:any;
  public topicId:any;
  public FlashcardList :any;
  constructor(
    private dialogService: NbDialogService,
    public FlashcardService : FlashcardService,
    public af:AngularFirestore,
    public ApplicationService : ApplicationServiceService,
    public SubjectService: SubjectService,
    public TopicService:TopicService
  ) { }

  ngOnInit() {
    console.log("ngOnInit")
    let topicList = this.af.collection(this.TopicService.path).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as any;
        const docId = a.payload.doc.id;
        data.selected=false
        return { docId, ...data };
      }))
    );
    topicList.subscribe(r => {
      this.TopicList = r;
    }) 

    let flash= this.af.collection(this.FlashcardService.path).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as any;
        const docId = a.payload.doc.id;
        data.selected=false
        return { docId, ...data };
      }))
    );
    flash.subscribe(r => {
      this.FlashcardList =r
    }) 
  }

  Add() {
    console.log("add")
    this.newFlashcard = new FlashcardModel();
    this.newFlashcard['type'] = 'Add';
    this.newFlashcard['TopicList'] = this.TopicList
    this.dialogService.open(NewFlashcardComponent,{context:{
      InfoData:this.newFlashcard
    }})
  }
  Update(){
    console.log("update")
    this.newFlashcard['type'] = 'update';
    this.newFlashcard['TopicList'] = this.TopicList
    this.dialogService.open(NewFlashcardComponent,{context: {
      InfoData:this.newFlashcard
    }})
  }
  deleteConfirm(){
    console.log("deleteConfirm")
    if(confirm("Are you sure to delete Flashcard")) {      
      this.FlashcardService.delete(this.newFlashcard.docId).then(() => { this.newFlashcard = new FlashcardModel(), console.log('data deleted successfully') })
    }
  }
  checked(checkmark){
    console.log("checked")
     for (let index = 0; index < this.FlashcardList.length; index++) {
      const element: any = this.FlashcardList[index];
      element.selected = false
    }
    checkmark.selected = !checkmark.selected;
    this.newFlashcard = checkmark;
  }

}
