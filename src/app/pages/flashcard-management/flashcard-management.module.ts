import { NgModule } from '@angular/core';
import {
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbInputModule,
    NbSelectModule,
} from '@nebular/theme';

import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FlashcardManagementComponent } from './flashcard-management.component';
import { NewFlashcardComponent } from './new-flashcard/new-flashcard.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NbCardModule,
        NbButtonModule,
        NbCheckboxModule,
        NbInputModule,
        NbSelectModule
    ],
    declarations: [
        FlashcardManagementComponent,
        NewFlashcardComponent,
        
    ],
    entryComponents: [NewFlashcardComponent,],
    providers:[]
})
export class FlashcardManagementModule { }