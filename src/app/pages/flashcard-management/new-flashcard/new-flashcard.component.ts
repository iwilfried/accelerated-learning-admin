import { Component, OnInit,ViewEncapsulation ,Input, } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { FlashcardModel } from '../../../shared/model/flashcard-model'
import { FlashcardService } from '../../../shared/service/firebase-api/flashcard/flashcard-service'

import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-new-flashcard',
  templateUrl: './new-flashcard.component.html',
  styleUrls: ['./new-flashcard.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class NewFlashcardComponent implements OnInit {

  @Input()  InfoData  : any;
  public newFlashcard: FlashcardModel;
  public items:any
  constructor(
    public ref: NbDialogRef<NewFlashcardComponent>,
    public FlashcardService : FlashcardService,
    
    public af:AngularFirestore
  ) { }

  close() {
    console.log('close')
    this.ref.close();
  }
  ngOnInit() {
    console.log('ngOnInit')
    this.items = this.InfoData.TopicList
    if(this.InfoData.type == 'Add'){
      this.newFlashcard = new FlashcardModel
    }else{
      this.newFlashcard = this.InfoData      
    }
  }
  submitNewPoint(name) {
    console.log('submitNewPoint')
    delete this.newFlashcard['TopicList'];
    if(!!this.InfoData && this.InfoData.type == 'update'){
      delete this.newFlashcard['type'];
      if(this.newFlashcard.topicId){
        this.FlashcardService.update(this.newFlashcard).then(() => {
          console.log('successfully updated')
        }).catch(err =>{
          console.log(err)
        })
        this.ref.close(name)
      }else{
        console.log("Please Select Topic")
      } 
    }else{
      if(this.newFlashcard.topicId){
        this.FlashcardService.addModel(this.newFlashcard).then(() => {
          console.log('successfully Added')
        }).catch(err =>{
          console.log(err)
        })
        this.ref.close(name)
      }else{
        console.log("Please Select Topic")
      } 
    }
  }

}
