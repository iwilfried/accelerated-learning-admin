import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFlashcardComponent } from './new-flashcard.component';

describe('NewFlashcardComponent', () => {
  let component: NewFlashcardComponent;
  let fixture: ComponentFixture<NewFlashcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFlashcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFlashcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
