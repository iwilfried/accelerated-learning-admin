import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'Dash Board',
        icon: 'monitor-outline',
        link: '/pages/dashboard',
        home: true,
    },
    {
        title: 'Application Management',
        icon: 'monitor-outline',
        link: '/pages/application',
        home: true,
    },
    {
        title: 'Subject Management',
        icon: 'monitor-outline',
        link: '/pages/subject',
        home: true,
    },
    {
        title: 'Topic Management',
        icon: 'monitor-outline',
        link: '/pages/topic',
        home: true,
    },
    {
        title: 'FlashCard Management',
        icon: 'monitor-outline',
        link: '/pages/flashcard',
        home: true,
    },
];
