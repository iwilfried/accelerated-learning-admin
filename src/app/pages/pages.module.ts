import { NgModule } from '@angular/core';
import { NbMenuModule, NbLayoutModule, NbSidebarModule, NbButtonModule, NbDialogModule } from '@nebular/theme';

import { PagesComponent } from './pages.component';

import { PagesRoutingModule } from './pages-routing.module';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ApplicationManagementComponent } from './application-management/application-management.component';
import { SubjectManagementComponent } from './subject-management/subject-management.component';
import { TopicManagementComponent } from './topic-management/topic-management.component';
import { FlashcardManagementComponent } from './flashcard-management/flashcard-management.component';
import { ApplicationManagementModule } from './application-management/application-management.module'
import { SubjectManagementModule } from './subject-management/subject-management.module'
import { TopicManagementModule } from './topic-management/topic-management.module';
import { FlashcardManagementModule } from './flashcard-management/flashcard-management.module'

@NgModule({
    imports: [
        PagesRoutingModule,
        NbMenuModule.forRoot(),
        ApplicationManagementModule,
        SubjectManagementModule,
        TopicManagementModule,
        FlashcardManagementModule,
        NbLayoutModule,
        

        NbEvaIconsModule,

        NbSidebarModule.forRoot(), // NbSidebarModule.forRoot(), //if this is your app.module
        NbButtonModule,
        NbDialogModule.forRoot(),
    ],
    declarations: [
        PagesComponent,
        DashboardComponent,
        // SubjectManagementComponent,
        // TopicManagementComponent,
        // FlashcardManagementComponent,

    ],
})
export class PagesModule {
}