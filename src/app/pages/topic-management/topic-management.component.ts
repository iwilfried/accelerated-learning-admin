import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { NewTopicComponent } from './new-topic/new-topic.component';
import { TopicModel } from '../../shared/model/topic-model'
import { TopicService } from '../../shared/service/firebase-api/topic/topic-service'

import { ApplicationServiceService } from '../../shared/service/firebase-api/application/application-service.service'
import { SubjectService } from '../../shared/service/firebase-api/subject/subject-service'
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-topic-management',
  templateUrl: './topic-management.component.html',
  styleUrls: ['./topic-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopicManagementComponent implements OnInit {

  public newTopic: TopicModel
  public TopicList: Array<TopicModel>;

  // public applicationList:any;
  public check: any;
  public SubjectList: any;
  public subjectId :any;
  constructor(
    private dialogService: NbDialogService,
    public TopicService: TopicService,
    public af: AngularFirestore,
    public ApplicationService: ApplicationServiceService,
    public SubjectService: SubjectService
  ) { }

  ngOnInit() {
    console.log("ngOnInit")   
    let subjectList = this.af.collection(this.SubjectService.path).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as any;
        const docId = a.payload.doc.id;
        return { docId, ...data };
      }))
    );
    subjectList.subscribe(r => {
      this.SubjectList = r;      
    })
    // Get Topic list

    let topics = this.af.collection(this.TopicService.path).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as any;
        const docId = a.payload.doc.id;
        data.selected = false
        return { docId, ...data };
      }))
    );
    topics.subscribe(r => {
      this.TopicList = r;
    })


  }
  checked(checkmark) {
    console.log("checked")
    for (let index = 0; index < this.TopicList.length; index++) {
      const element: any = this.TopicList[index];
      element.selected = false
    }
    checkmark.selected = !checkmark.selected;
    this.newTopic = checkmark;
  }

  Add() {
    console.log("add")
    for (let index = 0; index < this.TopicList.length; index++) {
      const element: any = this.TopicList[index];
      element.selected = false
    }
    this.newTopic = new TopicModel();
    this.newTopic['type'] = 'Add';
    this.newTopic['SubjectList'] = this.SubjectList
    this.newTopic['TopicList'] = this.TopicList

    this.dialogService.open(NewTopicComponent, {
      context: {
        InfoData: this.newTopic
      }
    })
  }
  Update() {
    console.log("update")
    this.newTopic['type'] = 'update';
    this.newTopic['SubjectList'] = this.SubjectList
    this.newTopic['TopicList'] = this.TopicList
    this.dialogService.open(NewTopicComponent, {
      context: {
        InfoData: this.newTopic
      }
    })

  }
  deleteConfirm() {
    console.log("deleteConfirm")
    if (confirm("Are you sure to delete subject")) {
       this.TopicService.delete(this.newTopic.docId).then(() => {
        this.newTopic = new TopicModel();
          console.log('data deleted successfully')
        })
     }
    }  
}
