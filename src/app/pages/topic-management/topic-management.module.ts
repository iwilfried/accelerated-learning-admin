import { NgModule } from '@angular/core';
import {
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbInputModule,
    NbSelectModule,
} from '@nebular/theme';

import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TopicManagementComponent } from './topic-management.component';
import { NewTopicComponent } from './new-topic/new-topic.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NbCardModule,
        NbButtonModule,
        NbCheckboxModule,
        NbInputModule,
        NbSelectModule
    ],
    declarations: [
        TopicManagementComponent,
        NewTopicComponent,
    ],
    entryComponents: [NewTopicComponent,],
    providers:[]
})
export class TopicManagementModule { }