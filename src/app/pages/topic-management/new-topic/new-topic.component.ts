import { Component, OnInit, ViewEncapsulation, Input, } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { TopicModel } from '../../../shared/model/topic-model'
import { TopicService } from '../../../shared/service/firebase-api/topic/topic-service'
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { SubjectService } from '../../../shared/service/firebase-api/subject/subject-service'

@Component({
  selector: 'app-new-topic',
  templateUrl: './new-topic.component.html',
  styleUrls: ['./new-topic.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewTopicComponent implements OnInit {
  InfoData: any;
  public newTopic: TopicModel;

  public Subject_id: any
  private parentList = [];
  public SubList: any
  public items =[]


  constructor(
    public ref: NbDialogRef<NewTopicComponent>,
    public TopicService: TopicService,
    public SubjectService: SubjectService,
    public af: AngularFirestore
  ) { }

  ngOnInit() {
    console.log("ngOnInit")
    this.SubList = this.InfoData.SubjectList
    this.parentList = this.items = this.InfoData.TopicList
    
    if (this.InfoData.type == 'Add') {
      this.newTopic = new TopicModel

    } else {
      this.newTopic = this.InfoData
      console.log(this.InfoData)

      this.checkParent();

    }

  }
  close() {
    console.log("close")
    this.ref.close();
    this.newTopic = new TopicModel()
    this.parentList = []
    this.SubList = [];
  }
  submitNewPoint(name) {
    console.log("submitNewPoint")
    delete this.newTopic['SubjectList'];
    delete this.newTopic['TopicList'];
    if (!!this.InfoData && this.InfoData.type == 'update') {
      
    delete this.newTopic['type'];
      if (this.newTopic.subject_id) {
        this.TopicService.update(this.newTopic).then(() => {
          console.log('successfully updated')
        }).catch(err => {
          console.log(err)
        })
        this.ref.close(name)
        this.newTopic = new TopicModel()
      } else {
        console.log("Please Select Subject")
      }
    } else {
      if (this.newTopic.subject_id) {
        this.TopicService.addModel(this.newTopic).then(() => {
          console.log('successfully Added')
        }).catch(err => {
          console.log(err)
        })
        this.ref.close(name)
      } else {
        console.log("Please Select Subject")
      }
    }
  }

  AddRemoveParents(events) {
    console.log("AddRemoveParents")
    if (!!this.newTopic.parents && this.newTopic.parents.length > 0) {
      // this.newTopic.parents.push(events.docId)
      var result = this.newTopic.parents.filter(hero => {
        return hero == events.docId;
      });
      if (!!result) {
        this.newTopic.parents.splice(0, 1);
      }
    } else {
      this.newTopic.parents = [events.docId]
    }

  }

  checkValue(docId) {
    this.newTopic.parents.forEach(element => {
      if (element == docId) {
        console.log(true)
        return true;
      }
    });
  }
  checkParent() {
    if (this.newTopic.parents && this.newTopic.parents.length > 0) {
      if (this.newTopic.parents.length > 1) {

      } else {
        this.parentList.forEach(element => {
          if (element.docId === this.newTopic.parents[0]) {
            element.parentsselected = true;
          } else {
            element.parentsselected = false;
          }
        });
      }
    } else {
      for (let index = 0; index < this.parentList.length; index++) {
        const element: any = this.parentList[index];
        element.parentsselected = false;

      }
    }
    console.log(this.parentList)

  }
}