import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ApplicationManagementComponent } from './application-management/application-management.component';
import { SubjectManagementComponent } from './subject-management/subject-management.component';
import { TopicManagementComponent } from './topic-management/topic-management.component'
import { FlashcardManagementComponent } from './flashcard-management/flashcard-management.component'

const routes: Routes = [{
    path: '',
    component: PagesComponent,
    children: [

        {
            path: 'dashboard',
            component: DashboardComponent,
        },
        {
            path: 'application',
            component: ApplicationManagementComponent,
        },
        {
            path: 'subject',
            component: SubjectManagementComponent,
        },
        {
            path: 'topic',
            component: TopicManagementComponent,
        },
        {
            path: 'flashcard',
            component: FlashcardManagementComponent,
        },
        {
            path: '',
            redirectTo: 'dashboard',
            pathMatch: 'full',
        },


    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesRoutingModule {
}