import { NgModule } from '@angular/core';
import {
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbInputModule,
    NbSelectModule,
} from '@nebular/theme';

import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SubjectManagementComponent } from './subject-management.component';
import { NewSubjectComponent } from './new-subject/new-subject.component'
// import { NewApplicationComponent } from './new-application/new-application.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NbCardModule,
        NbButtonModule,
        NbCheckboxModule,
        NbInputModule,
        NbSelectModule
    ],
    declarations: [
        SubjectManagementComponent,
        NewSubjectComponent,

    ],
    entryComponents: [NewSubjectComponent,],
    providers:[]
})
export class SubjectManagementModule { }