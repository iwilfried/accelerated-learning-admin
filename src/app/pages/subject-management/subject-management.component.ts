import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { NewSubjectComponent } from './new-subject/new-subject.component';
import { SubjectModel } from '../../shared/model/subject-model'
import { SubjectService } from '../../shared/service/firebase-api/subject/subject-service'

import { ApplicationServiceService } from '../../shared/service/firebase-api/application/application-service.service'
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-subject-management',
  templateUrl: './subject-management.component.html',
  styleUrls: ['./subject-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubjectManagementComponent implements OnInit {

  public newSubject: SubjectModel
  public SubjectList:any = [];
  public applicationList:any = [];
  public appId:any;
  constructor(
    private dialogService: NbDialogService,
    public SubjectService : SubjectService,
    public af:AngularFirestore,
    public ApplicationService : ApplicationServiceService,
  ) { }

  
  ngOnInit() {
    console.log("ngOnInit")
     let applIst = this.af.collection(this.ApplicationService.path).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as any;
        const docId = a.payload.doc.id;
        data.selected=false
        return { docId, ...data };
      }))
    );
    applIst.subscribe(r => {
      this.applicationList = r;
    }) 
    let subjectlist = this.af.collection(this.SubjectService.path).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as any;
        const docId = a.payload.doc.id;
        data.selected=false
        return { docId, ...data };
      }))
    );
    subjectlist.subscribe(r => {
      this.SubjectList = r;
    }) 
  }
  Add() {
    console.log("add")
    this.newSubject = new SubjectModel();
    this.newSubject['type'] = 'Add';
    this.newSubject['appList'] = this.applicationList
    this.dialogService.open(NewSubjectComponent, { context: {
      InfoData:this.newSubject
    }})
  }

  Update(){
    console.log("update")
    this.newSubject['type'] = 'update';
    this.newSubject['appList'] = this.applicationList
    this.dialogService.open(NewSubjectComponent,{context: {
      InfoData:this.newSubject
    }})
  }
  checked(checkmark){
    console.log("checked")
    for (let index = 0; index < this.SubjectList.length; index++) {
      const element: any = this.SubjectList[index];
      element.selected = false
    }
    checkmark.selected = !checkmark.selected;
    this.newSubject = checkmark;
  }
  deleteConfirm(){
    console.log("deleteConfirm")
    if(confirm("Are you sure to delete subject")) {      
      this.SubjectService.delete(this.newSubject.docId).then(() => {
        this.newSubject = new SubjectModel();
         console.log('data deleted successfully') })

    }
  }
  

}
