import { Component, OnInit,ViewEncapsulation ,Input, } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { SubjectModel } from '../../../shared/model/subject-model'
import { SubjectService } from '../../../shared/service/firebase-api/subject/subject-service'

import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-new-subject',
  templateUrl: './new-subject.component.html',
  styleUrls: ['./new-subject.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewSubjectComponent implements OnInit {
  @Input()  InfoData  : any;
  public newSubject: SubjectModel;
  public items:any
  constructor(
    public ref: NbDialogRef<NewSubjectComponent>,
    public SubjectService : SubjectService,
    
    public af:AngularFirestore
  ) { }

  ngOnInit() {
    console.log("ngOnInit")
    this.items = this.InfoData.appList

    if(this.InfoData.type == 'Add'){
      this.newSubject = new SubjectModel
    }else{
      this.newSubject = this.InfoData
    }
  }
  
  close() {
    console.log("close")
    this.ref.close();
  }
  submitNewPoint(name) {
    console.log("submitNewPoint")
    delete this.newSubject['appList'];
    if(!!this.InfoData && this.InfoData.type == 'update'){
      delete this.newSubject['type'];
      if(this.newSubject.application_id){
        this.SubjectService.update(this.newSubject).then(() => {
          console.log('successfully updated')
        }).catch(err =>{
          console.log(err)
        })
        this.ref.close(name)
      }else{
        console.log("Please Select Application")
      } 
    }else{
      if(this.newSubject.application_id){
        this.SubjectService.addModel(this.newSubject).then(() => {
          console.log('successfully Added')
        }).catch(err =>{
          console.log(err)
        })
        this.ref.close(name)
      }else{
        console.log("Please Select Application")
      } 
    }
  }

}
