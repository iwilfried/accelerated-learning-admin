import { ErrorHandler, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyErrorService implements ErrorHandler {


  constructor() { }
  handleError(error: any): void {
    console.log(error);

  }
}
