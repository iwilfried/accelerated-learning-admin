import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { FlashcardModel } from '../../../model/flashcard-model'
import { AngularFirestore } from '@angular/fire/firestore';
import { MyErrorService } from '../my-error.service';
@Injectable({
    providedIn: 'root'
  })
export class FlashcardService extends BaseService <FlashcardModel>{
    constructor(public db: AngularFirestore,public myErr: MyErrorService) {
        super(db, myErr, 'Flashcard');
      }
}
