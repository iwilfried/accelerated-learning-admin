import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { SubjectModel } from '../../../model/subject-model'
import { AngularFirestore } from '@angular/fire/firestore';
import { MyErrorService } from '../my-error.service';
import { map } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
  })
export class SubjectService extends BaseService <SubjectModel>{
    constructor(public db: AngularFirestore,public myErr: MyErrorService) {
      super(db, myErr, 'Subject');
    }
}