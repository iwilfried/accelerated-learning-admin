import { TestBed } from '@angular/core/testing';

import { MyErrorService } from './my-error.service';

describe('MyErrorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyErrorService = TestBed.get(MyErrorService);
    expect(service).toBeTruthy();
  });
});
