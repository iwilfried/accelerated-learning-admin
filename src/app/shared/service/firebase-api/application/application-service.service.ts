import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { ApplicationModel } from '../../../model/application-model'
import { AngularFirestore } from '@angular/fire/firestore';
import { MyErrorService } from '../my-error.service';
@Injectable({
  providedIn: 'root'
})
export class ApplicationServiceService extends BaseService <ApplicationModel>{

  constructor(public db: AngularFirestore,public myErr: MyErrorService) {
    super(db, myErr, 'Application');
   }
}
