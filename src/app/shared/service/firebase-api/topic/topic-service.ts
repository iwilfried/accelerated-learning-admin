import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { TopicModel } from '../../../model/topic-model'
import { AngularFirestore } from '@angular/fire/firestore';
import { MyErrorService } from '../my-error.service';
@Injectable({
    providedIn: 'root'
  })
export class TopicService extends BaseService <TopicModel>{
    constructor(public db: AngularFirestore,public myErr: MyErrorService) {
      super(db, myErr, 'Topic');
    }
    
}
