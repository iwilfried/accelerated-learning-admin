export class ApplicationModel {
    public docId?: string;
    public application_name:string;
    constructor(model:any={}) {
        this.docId=model.docId;
        this.application_name=model.application_name;
    }
}
