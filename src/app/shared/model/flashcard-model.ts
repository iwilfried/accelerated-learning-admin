export class FlashcardModel {
    public docId?: string;
    public topicId:any;
    public front: string;
    public back: string;
    constructor(model:any={}) {
        this.docId=model.docId;
        this.topicId = model.topicId
        this.front=model.front;
        this.back = model.back;
    }
}
