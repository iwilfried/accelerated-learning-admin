// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    /* apiKey: 'AIzaSyCHsR7HlpFl7-6JjKYsRfZ6ykSOi5P-f6E',
    authDomain: 'fruit-b930e.firebaseapp.com',
    databaseURL: 'https://fruit-b930e.firebaseio.com',
    projectId: 'fruit-b930e',
    storageBucket: 'fruit-b930e.appspot.com',
    messagingSenderId: '785940413726',
    appId: '1:785940413726:web:cd9ea2b1bd93e2e6' */
    apiKey: "AIzaSyAIdZ8iLbenzj7z1AFcj3LDxhBGaCIweyk",
    authDomain: "fruitbase-admin-ionic.firebaseapp.com",
    databaseURL: "https://fruitbase-admin-ionic.firebaseio.com",
    projectId: "fruitbase-admin-ionic",
    storageBucket: "fruitbase-admin-ionic.appspot.com",
    messagingSenderId: "1016143056482",
    appId: "1:1016143056482:web:f4f9d8e70ab93046144b3b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
